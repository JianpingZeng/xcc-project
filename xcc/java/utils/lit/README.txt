===============================
 lit - A Software Testing Tool
===============================

lit is a portable tool for executing XCC and Jlang style test suites,
summarizing their results, and providing indication of failures. lit is designed
to be a lightweight testing tool with as simple a user interface as possible.

