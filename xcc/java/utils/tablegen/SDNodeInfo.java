package utils.tablegen;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

import backend.codegen.MVT;
import tools.Error;
import tools.Util;
import utils.tablegen.Init.DefInit;

import java.util.ArrayList;
import java.util.Map;

import static utils.tablegen.SDNodeInfo.SDTypeConstraint.constraintType.*;
import static utils.tablegen.SDNodeProperties.parseSDPatternOperatorProperties;
import static utils.tablegen.ValueTypeByHwMode.getValueTypeByHwMode;

/**
 * @author Jianping Zeng
 * @version 0.4
 */
public final class SDNodeInfo {
  private Record theDef;
  private String enumName;
  private String sdClassName;
  private int properties;
  private int numResults;
  private int numOperands;
  private ArrayList<SDTypeConstraint> typeConstraints;
  private CodeGenHwModes hwModes;

  public SDNodeInfo(Record r, CodeGenHwModes hwModes) {
    theDef = r;
    enumName = r.getValueAsString("Opcode");
    sdClassName = r.getValueAsString("SDClass");
    Record typeProfile = r.getValueAsDef("TypeProfile");
    numResults = (int) typeProfile.getValueAsInt("NumResults");
    numOperands = (int) typeProfile.getValueAsInt("NumOperands");
    this.hwModes = hwModes;
    properties = parseSDPatternOperatorProperties(r);

    // Parse the type constraints.
    typeConstraints = new ArrayList<>();
    ArrayList<Record> constrainsts = typeProfile.getValueAsListOfDefs("Constraints");
    for (Record con : constrainsts) {
      typeConstraints.add(new SDTypeConstraint(con, hwModes));
    }
  }

  public int getNumResults() {
    return numResults;
  }

  public int getNumOperands() {
    return numOperands;
  }

  public Record getRecord() {
    return theDef;
  }

  public String getEnumName() {
    return enumName;
  }

  public String getSDClassName() {
    return sdClassName;
  }

  public ArrayList<SDTypeConstraint> getTypeConstraints() {
    return typeConstraints;
  }

  /**
   * Return true if this node has the specified property.
   *
   * @param prop
   * @return
   */
  public boolean hasProperty(int prop) {
    return (properties & (1 << prop)) != 0;
  }

  /**
   * Given a node in a pattern, apply the type
   * constraints for this node to the operands of the node.  This returns
   * true if it makes a change, false otherwise.  If a type contradiction is
   * found, throw an exception.
   *
   * @param node
   * @param tp
   * @return
   */
  public boolean applyTypeConstraints(TreePatternNode node, TreePattern tp) {
    boolean changed = false;
    for (SDTypeConstraint con : typeConstraints) {
      changed |= con.applyTypeConstraint(node, this, tp);
    }
    return changed;
  }

  public int getKnownType(int resNo) {
    int numResults = getNumResults();
    Util.assertion(numResults <= 1, "we only work with nodes with at most one result so far!");
    Util.assertion(resNo == 0, "only handles single result node as yet!");

    for (SDTypeConstraint cst : typeConstraints) {
      if (cst.operandNo >= numResults)
        continue;
      switch (cst.constraintType) {
        default:
          break;
        case SDTCisVT:
          if (cst.vvt.isSimple())
            return cst.vvt.getSimple().simpleVT;
          break;
        case SDTCisPtrTy:
          return MVT.iPTR;
      }
    }
    return MVT.Other;
  }

  public static final class SDTypeConstraint {
    public SDTypeConstraint(Record r, CodeGenHwModes hwModes) {
      operandNo = (int) r.getValueAsInt("OperandNum");

      if (r.isSubClassOf("SDTCisVT")) {
        constraintType = SDTCisVT;
        vvt = getValueTypeByHwMode(r.getValueAsDef("VT"), hwModes);
        for (Map.Entry<Integer, MVT> itr : vvt.map.entrySet()) {
          if (itr.getValue().simpleVT == MVT.isVoid)
            Error.printFatalError(r.getLoc(), "Can't use 'Void' as type to SDTCisVT");
        }
      } else if (r.isSubClassOf("SDTCisPtrTy")) {
        constraintType = SDTCisPtrTy;
      } else if (r.isSubClassOf("SDTCisInt")) {
        constraintType = SDTCisInt;
      } else if (r.isSubClassOf("SDTCisFP")) {
        constraintType = SDTCisFP;
      } else if (r.isSubClassOf("SDTCisVec")) {
        constraintType = SDTCisVec;
      } else if (r.isSubClassOf("SDTCisSameAs")) {
        constraintType = SDTCisSameAs;
        x = (int) r.getValueAsInt("OtherOperandNum");
      } else if (r.isSubClassOf("SDTCisVTSmallerThanOp")) {
        constraintType = SDTCisVTSmallerThanOp;
        x = (int) r.getValueAsInt("OtherOperandNum");
      } else if (r.isSubClassOf("SDTCisOpSmallerThanOp")) {
        constraintType = SDTCisOpSmallerThanOp;
        x = (int) r.getValueAsInt("BigOperandNum");
      } else if (r.isSubClassOf("SDTCisEltOfVec")) {
        constraintType = SDTCisEltOfVec;
        x = (int) r.getValueAsInt("OtherOpNum");
      } else if (r.isSubClassOf("SDTCisSubVecOfVec")) {
        constraintType = SDTCisSubVecOfVec;
        x = (int)r.getValueAsInt("OtherOpNum");
      }
      else {
        Error.printFatalError("Unrecognized SDTypeConstraint '" + r.getName() + "'!");
      }
    }

    public int operandNo;

    public enum constraintType {
      SDTCisVT,
      SDTCisPtrTy,
      SDTCisInt,
      SDTCisFP,
      SDTCisVec,
      SDTCisSameAs,
      SDTCisVTSmallerThanOp,
      SDTCisOpSmallerThanOp,
      SDTCisEltOfVec,
      SDTCisSubVecOfVec
    }

    public constraintType constraintType;

    /**
     * For represents the SDTCisVT_info, SDTCisSameAs_Info,
     * SDTCisVTSmallerThanOp_Info, SDTCisOpSmallerThanOp_Info,
     * SDTCisEltOfVec_Info.
     */
    public int x;
    /**
     * The VT for SDTCisVT and SDTCVecEltisVT.
     */
    public ValueTypeByHwMode vvt;

    /**
     * Given a node in a pattern, apply this type constraint to the nodes operands.
     * This returns true if it makes a change, false otherwise.
     * If a type contradiction is found, issue an error.
     *
     * @param node
     * @param nodeInfo
     * @param tp
     * @return
     */
    public boolean applyTypeConstraint(TreePatternNode node,
                                       SDNodeInfo nodeInfo,
                                       TreePattern tp) {
      int numResults = nodeInfo.getNumResults();
      TypeInfer infer = tp.getTypeInfer();
      CodeGenTarget cgt = tp.getDAGPatterns().getTarget();
      int[] tempResNo = new int[1];
      TreePatternNode nodeToApply = getOperandNum(operandNo, node, numResults, tempResNo);
      int resNo = tempResNo[0]; // The result number being referenced.

      switch (constraintType) {
        default:
          Util.assertion("Unknown constraint yet!");
        case SDTCisVT:
          return nodeToApply.updateNodeType(resNo, vvt, tp);
        case SDTCisPtrTy:
          // Require it to be one of the legal FP VTs.
          return nodeToApply.updateNodeType(resNo, MVT.iPTR, tp);
        case SDTCisInt: {
          // Require it to be one of the legal integer VTs.
          return infer.enforceInteger(nodeToApply.getExtType(resNo));
        }
        case SDTCisFP: {
          // Require it to be one of the legal floating point VTs.
          return infer.enforceFloatingPoint(nodeToApply.getExtType(resNo));
        }
        case SDTCisVec: {
          // Require it to be one of the legal vector VTs.
          return infer.enforceVector(nodeToApply.getExtType(resNo));
        }
        case SDTCisSameAs: {
          // Require it to be one of the legal floating point VTs.
          int[] oResNo = new int[1];
          TreePatternNode otherNode = getOperandNum(x, node, numResults, oResNo);
          return nodeToApply.updateNodeType(resNo, otherNode.getExtType(oResNo[0]), tp) |
              otherNode.updateNodeType(oResNo[0], nodeToApply.getExtType(resNo), tp);
        }
        case SDTCisVTSmallerThanOp: {
          if (!nodeToApply.isLeaf() || !(nodeToApply.getLeafValue() instanceof DefInit)
              || !((DefInit) nodeToApply.getLeafValue()).getDef().isSubClassOf("ValueType")) {
            tp.error(node.getOperator().getName() + " expects a VT operand!");
          }

          DefInit di = ((DefInit) nodeToApply.getLeafValue());
          vvt = getValueTypeByHwMode(di.getDef(), cgt.getHwModes());
          TypeSetByHwMode set = new TypeSetByHwMode(vvt);
          int[] oResNo = new int[1];
          TreePatternNode otherNode = getOperandNum(x, node, numResults, oResNo);
          return infer.enforceSmallerThan(set, otherNode.getExtType(oResNo[0]));
        }
        case SDTCisOpSmallerThanOp: {
          int[] bResNo = new int[1];
          TreePatternNode bigOp = getOperandNum(x, node, numResults, bResNo);
          return infer.enforceSmallerThan(nodeToApply.getExtType(resNo),
              bigOp.getExtType(bResNo[0]));
        }
        case SDTCisEltOfVec: {
          int[] vResNo = new int[1];
          TreePatternNode bigVecOp = getOperandNum(x, node, numResults, vResNo);
          return infer.enforceVectorEltTypeIs(bigVecOp.getExtType(vResNo[0]),
              nodeToApply.getExtType(resNo));
        }
        case SDTCisSubVecOfVec: {
          int[] vResNo = new int[1];
          TreePatternNode bigVecOp = getOperandNum(x, node, numResults, vResNo);
          return infer.enforceVectorSubVectorTypesIs(bigVecOp.getExtType(vResNo[0]),
                  nodeToApply.getExtType(resNo));
        }
      }
    }

    /**
     * Return the node corresponding to operand {@code opNo} in tree
     *
     * @param opNo
     * @param node
     * @param numResults
     * @return
     * @{code node}, which has {@code numResults} results.
     */
    public TreePatternNode getOperandNum(int opNo,
                                         TreePatternNode node,
                                         int numResults,
                                         int[] resNo) {
      if (opNo < numResults) {
        resNo[0] = opNo;
        return node;
      }

      opNo -= numResults;

      if (opNo >= node.getNumChildren()) {
        System.err.printf("Invalid operand number %d ", opNo + numResults);
        node.dump();
        System.err.println();
        System.exit(1);
      }
      return node.getChild(opNo);
    }
  }
}
