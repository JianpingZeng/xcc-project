package utils.tablegen;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

import backend.codegen.EVT;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TObjectIntHashMap;
import tools.Error;
import tools.Pair;
import tools.Util;
import utils.tablegen.CodeGenInstruction.OperandInfo;
import utils.tablegen.Init.DagInit;
import utils.tablegen.Init.DefInit;
import utils.tablegen.Init.ListInit;
import utils.tablegen.Init.TypedInit;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import static utils.tablegen.CodeGenHwModes.DefaultMode;
import static utils.tablegen.SDNP.SDNPAssociative;
import static utils.tablegen.SDNP.SDNPCommutative;
import static utils.tablegen.TGParser.resolveTypes;
import static utils.tablegen.TreePattern.getNumNodeResults;

/**
 * @author Jianping Zeng
 * @version 0.4
 */
public final class CodeGenDAGPatterns {
  private RecordKeeper records;
  private CodeGenTarget target;
  private ArrayList<CodeGenIntrinsic> intrinsics;
  private ArrayList<CodeGenIntrinsic> tgtIntrinsics;

  /**
   * Deterministic comparison of Record.
   */
  private static Comparator<Record> RecordCmp = Comparator.comparingInt(Record::getID);

  private TreeMap<Record, SDNodeInfo> sdnodes;
  private TreeMap<Record, Pair<Record, String>> sdNodeXForms;
  private TreeMap<Record, ComplexPattern> complexPatterns;
  private TreeMap<Record, TreePattern> patternFragments;
  private TreeMap<Record, DAGDefaultOperand> defaultOperands;
  private TreeMap<Record, DAGInstruction> instructions;

  private Record intrinsicVoidSDNode;
  private Record intrinsicWChainSDNode;
  private Record intrinsicWOChainSDNode;
  private ArrayList<PatternToMatch> patternsToMatch;
  private TypeSetByHwMode legalValueTypes;

  public CodeGenDAGPatterns(RecordKeeper records) {
    this.records = records;
    target = new CodeGenTarget(records);
    defaultOperands = new TreeMap<>(RecordCmp);
    patternFragments = new TreeMap<>(RecordCmp);
    complexPatterns = new TreeMap<>(RecordCmp);
    sdNodeXForms = new TreeMap<>(RecordCmp);
    sdnodes = new TreeMap<>(RecordCmp);
    instructions = new TreeMap<>(RecordCmp);
    patternsToMatch = new ArrayList<>();
    legalValueTypes = new TypeSetByHwMode(target.getLegalValueTypes());
    intrinsics = loadIntrinsics(records, false);
    tgtIntrinsics = loadIntrinsics(records, true);

    parseNodeInfo();
    parseNodeTransforms();
    parseComplexPatterns();
    parsePatternFragments();
    parseDefaultOperands();
    parseInstructions();
    parsePatterns();

    // Break patterns with parameterized types into a series of patterns,
    // where each one has a fixed type and is predicated on the conditions
    // of the associated HW mode.
    expandHwModeBasedTypes();

    verifyInstrTypes();

    // For example, commutative patterns can match multiple ways.Add them
    // to patternToMatch as well.
    generateVariants();

    // For example, we can detect loads, stores, and side effects in many
    // cases by examining an instruction's pattern.
    inferInstructionFlags();
  }

  /**
   * Return true if verification is correct.
   *
   * @param node
   * @return
   */
  private boolean verifyPatternNodeType(TreePatternNode node, boolean isRoot) {
    if (!isRoot && (node.getNumTypes() <= 0 || !node.getExtType(0).isMachineValueType())) {
      return false;
    }
    boolean res = true;
    for (int i = 0, e = node.getNumChildren(); i < e; i++)
      res &= verifyPatternNodeType(node.getChild(i), false);
    return res;
  }

  private void verifyInstrTypes() {
    for (PatternToMatch tp : patternsToMatch) {
      boolean succ = verifyPatternNodeType(tp.getSrcPattern(), true);
      succ &= verifyPatternNodeType(tp.getDstPattern(), true);
      if (!succ) {
        System.err.println();
        tp.dump();
        System.err.println();
        Util.assertion("There must be one and only one type remained");
      }
    }
  }

  TypeSetByHwMode getLegalValueTypes() { return legalValueTypes; }

  private void getInstructionInTree(TreePatternNode node, ArrayList<Record> instrs) {
    if (node.isLeaf())
      return;
    if (node.getOperator().isSubClassOf("Instruction"))
      instrs.add(node.getOperator());
    for (int i = 0, e = node.getNumChildren(); i < e; i++)
      getInstructionInTree(node.getChild(i), instrs);
  }

  private void inferInstructionFlags() {
    boolean error = false;
    for (PatternToMatch tp : patternsToMatch) {

      ArrayList<Record> patInstrs = new ArrayList<>();
      getInstructionInTree(tp.getDstPattern(), patInstrs);
      if (patInstrs.size() != 1)
        continue;

      CodeGenInstruction instInfo = target.getInstruction(patInstrs.get(0).getName());
      error |= inferFromPattern(instInfo, tp, this);
    }

    if (error)
      Error.printFatalError("Pattern conflicts");
  }

  private void collectModes(TreeSet<Integer> modes, TreePatternNode node) {
    for (TypeSetByHwMode vt : node.getExtTypes())
      modes.addAll(vt.map.keySet());

    for (int i = 0, e = node.getNumChildren(); i < e; i++)
      collectModes(modes, node.getChild(i));
  }

  private void expandHwModeBasedTypes() {
    CodeGenHwModes cgh = getTarget().getHwModes();
    TreeMap<Integer, ArrayList<utils.tablegen.Predicate>> modeChecks = new TreeMap<>();
    ArrayList<PatternToMatch> copy = new ArrayList<>();
    // FIXME, if it is failed to copy reference, replacing it with deeply copy.
    copy.addAll(patternsToMatch);
    patternsToMatch.clear();

    BiFunction<PatternToMatch, Integer, Void> appendPattern =
        (tp, mode) ->
        {
          TreePatternNode newSrc = tp.srcPattern.clone();
          TreePatternNode newDst = tp.dstPattern.clone();
          if (!newSrc.setDefaultMode(mode) || !newDst.setDefaultMode(mode))
            return null;

          ArrayList<utils.tablegen.Predicate> preds = new ArrayList<>(tp.preds);
          if (!modeChecks.containsKey(mode))
            modeChecks.put(mode, new ArrayList<>());
          preds.addAll(modeChecks.get(mode));
          patternsToMatch.add(new PatternToMatch(tp.getSrcRecord(), preds,
              newSrc, newDst, tp.getDstRegs(), tp.getAddedComplexity()));
          return null;
        };

    for (PatternToMatch tp : copy) {
      TreePatternNode src = null, dst = null;
      if (tp.srcPattern.hasProperTypeByHwMode())
        src = tp.srcPattern;
      if (tp.dstPattern.hasProperTypeByHwMode())
        dst = tp.dstPattern;

      if (src == null && dst == null) {
        patternsToMatch.add(tp);
        continue;
      }

      TreeSet<Integer> modes = new TreeSet<>();
      if (src != null)
        collectModes(modes, src);
      if (dst != null)
        collectModes(modes, dst);

      // The predicate for the default mode needs to be constructed for each
      // pattern separately.
      // Since not all modes must be present in each pattern, if a mode m is
      // absent, then there is no point in constructing a check for m. If such
      // a check was created, it would be equivalent to checking the default
      // mode, except not all modes' predicates would be a part of the checking
      // code. The subsequently generated check for the default mode would then
      // have the exact same patterns, but a different predicate code. To avoid
      // duplicated patterns with different predicate checks, construct the
      // default check as a negation of all predicates that are actually present
      // in the source/destination patterns.
      ArrayList<utils.tablegen.Predicate> defaultPred = new ArrayList<>();
      for (int m : modes) {
        if (m == DefaultMode)
          continue;
        if (modeChecks.containsKey(m))
          continue;

        // Fill the map entry for this mode.
        CodeGenHwModes.HwMode hm = cgh.getMode(m);
        ArrayList<utils.tablegen.Predicate> list = new ArrayList<>();
        list.add(new utils.tablegen.Predicate(hm.features, true));
        modeChecks.put(m, list);

        // Add negations of the HM's predicates to the default predicate.
        defaultPred.add(new utils.tablegen.Predicate(hm.features, false));
      }

      for (int m : modes) {
        if (m == DefaultMode)
          continue;
        appendPattern.apply(tp, m);
      }

      if (modes.contains(DefaultMode))
        appendPattern.apply(tp, DefaultMode);
    }
  }

  private static boolean inferFromPattern(CodeGenInstruction instInfo,
                                          PatternToMatch pat,
                                          CodeGenDAGPatterns cdp) {
    boolean mayStore, mayLoad, hasSideEffect;
    InstAnalyzer analyzer = new InstAnalyzer(cdp, false, false, false);
    analyzer.analyze(pat.getSrcPattern());
    mayStore = analyzer.mayStore;
    mayLoad = analyzer.mayLoad;
    hasSideEffect = analyzer.hasSideEffect;

    // TODO 9/29/2018
    boolean error = false;
    /*
    if (instInfo.mayStore != mayStore) {
      Error.printError(instInfo.theDef.getLoc(),
          String.format("Pattern doesn't match mayStore = %b",
          instInfo.mayStore));
      error = true;
    }

    if (instInfo.mayLoad != mayLoad) {
      Error.printError(instInfo.theDef.getLoc(),
          String.format("Pattern doesn't match mayLoad = %b",
              instInfo.mayLoad));
      error = true;
    }

    if (instInfo.hasSideEffects != hasSideEffect) {
      Error.printError(instInfo.theDef.getLoc(),
          String.format("Pattern doesn't match hasSideEffects = %b",
          instInfo.hasSideEffects));
      error = true;
    }*/

    instInfo.hasSideEffects |= hasSideEffect;
    instInfo.mayStore |= mayStore;
    instInfo.mayLoad |= mayLoad;
    return error;
  }

  /**
   * Generates variants. For example, commutative patterns can match multiple ways.
   * Add them to PatternsToMatch as well.
   */
  private void generateVariants() {
    if (TableGen.DEBUG)
      System.out.println("Generating instruction variants.");

    // Loop over all of the patterns we've collected, checking to see if we can
    // generate variants of the instruction, through the exploitation of
    // identities.  This permits the target to provide aggressive matching without
    // the .td file having to contain tons of variants of instructions.
    //
    // Note that this loop adds new patterns to the PatternsToMatch list, but we
    // intentionally do not reconsider these.  Any variants of added patterns have
    // already been added.
    for (int i = 0, e = patternsToMatch.size(); i != e; i++) {
      PatternToMatch match = patternsToMatch.get(i);

      HashSet<String> depVars = new HashSet<>();
      ArrayList<TreePatternNode> variants = new ArrayList<>();
      findDepVars(match.getSrcPattern(), depVars);
      if (TableGen.DEBUG) {
        System.out.print("Dependent/multiply used variables: ");
        dumpDepVars(depVars);
        System.out.println();
      }
      generateVariantsOf(match.getSrcPattern(), variants, this, depVars);

      Util.assertion(!variants.isEmpty(), "Must create at least original variant!");
      variants.remove(0);

      if (variants.isEmpty())
        continue;

      if (TableGen.DEBUG) {
        System.out.print("FOUND VARIANTS OF: ");
        match.getSrcPattern().dump();
        System.out.println();
      }

      for (int j = 0, sz = variants.size(); j != sz; j++) {
        TreePatternNode var = variants.get(j);
        if (TableGen.DEBUG) {
          System.out.print("\tVAR" + j + ": ");
          var.dump();
          System.out.println();
        }

        boolean alreadyExists = false;
        for (int p = 0, ee = patternsToMatch.size(); p < ee; p++) {
          if (match.getPredicates() != patternsToMatch.get(p).getPredicates())
            continue;

          if (var.isIsomorphicTo(patternsToMatch.get(p).getSrcPattern(), depVars)) {
            if (TableGen.DEBUG) {
              System.out.println("\t***ALREADY EXISTS, igoring variant.");
            }
            alreadyExists = true;
            break;
          }
        }

        if (alreadyExists) continue;

        addPatternsToMatch(new PatternToMatch(match.getSrcRecord(),
            match.getPredicates(), var,
            match.getDstPattern(), match.getDstRegs(),
            match.getAddedComplexity()));
      }
      if (TableGen.DEBUG)
        System.out.println();
    }
  }

  /**
   * Given a pattern N, generate all permutations we can of
   * the (potentially recursive) pattern by using algebraic laws.
   *
   * @param node
   * @param outVariants
   * @param cdp
   * @param depVars
   */
  private static void generateVariantsOf(TreePatternNode node,
                                         ArrayList<TreePatternNode> outVariants,
                                         CodeGenDAGPatterns cdp,
                                         HashSet<String> depVars) {
    if (node.isLeaf()) {
      outVariants.add(node);
      return;
    }

    SDNodeInfo nodeInfo = cdp.getSDNodeInfo(node.getOperator());

    // If this node is associative, re-associate.
    if (nodeInfo.hasProperty(SDNPAssociative)) {
      // Re-associate by pulling together all of the linked operators
      ArrayList<TreePatternNode> maximalChildren = new ArrayList<>();
      gatherChildrenOfAssociativeOpcode(node, maximalChildren);

      // Only handle child sizes of 3.  Otherwise we'll end up trying too many
      // permutations.
      if (maximalChildren.size() == 3) {
        // Find the variants of all of our maximal children.
        ArrayList<TreePatternNode> avariants = new ArrayList<>(),
            bvariants = new ArrayList<>(),
            cvariants = new ArrayList<>();

        generateVariantsOf(maximalChildren.get(0), avariants, cdp, depVars);
        generateVariantsOf(maximalChildren.get(1), bvariants, cdp, depVars);
        generateVariantsOf(maximalChildren.get(2), cvariants, cdp, depVars);

        // There are only two ways we can permute the tree:
        //   (A op B) op C    and    A op (B op C)
        // Within these forms, we can also permute A/B/C.

        // Generate legal pair permutations of A/B/C.
        ArrayList<TreePatternNode> abvariants = new ArrayList<>(),
            bavariants = new ArrayList<>(),
            acVariants = new ArrayList<>(),
            caVariants = new ArrayList<>(),
            bcVariants = new ArrayList<>(),
            cbVariants = new ArrayList<>();
        combineChildVariants(node, avariants, bvariants, abvariants, cdp, depVars);
        combineChildVariants(node, bvariants, avariants, bavariants, cdp, depVars);
        combineChildVariants(node, avariants, cvariants, acVariants, cdp, depVars);
        combineChildVariants(node, cvariants, avariants, caVariants, cdp, depVars);
        combineChildVariants(node, bvariants, cvariants, bcVariants, cdp, depVars);
        combineChildVariants(node, cvariants, bvariants, cbVariants, cdp, depVars);

        // Combine those into the result: (x op x) op x
        combineChildVariants(node, abvariants, cvariants, outVariants, cdp, depVars);
        combineChildVariants(node, bavariants, cvariants, outVariants, cdp, depVars);
        combineChildVariants(node, acVariants, bvariants, outVariants, cdp, depVars);
        combineChildVariants(node, caVariants, bvariants, outVariants, cdp, depVars);
        combineChildVariants(node, bcVariants, avariants, outVariants, cdp, depVars);
        combineChildVariants(node, cbVariants, avariants, outVariants, cdp, depVars);

        // Combine those into the result: x op (x op x)
        combineChildVariants(node, cvariants, abvariants, outVariants, cdp, depVars);
        combineChildVariants(node, cvariants, bavariants, outVariants, cdp, depVars);
        combineChildVariants(node, bvariants, acVariants, outVariants, cdp, depVars);
        combineChildVariants(node, bvariants, caVariants, outVariants, cdp, depVars);
        combineChildVariants(node, avariants, bcVariants, outVariants, cdp, depVars);
        combineChildVariants(node, avariants, cbVariants, outVariants, cdp, depVars);
        return;
      }
    }

    // Compute permutations of all children.
    ArrayList<ArrayList<TreePatternNode>> childVariants = new ArrayList<>(node.getNumChildren());
    for (int i = 0, e = node.getNumChildren(); i != e; i++) {
      ArrayList<TreePatternNode> list = new ArrayList<>();
      generateVariantsOf(node.getChild(i), list, cdp, depVars);
      childVariants.add(list);
    }

    // Build all permutations based on how the children were formed.
    combineChildVariants(node, childVariants, outVariants, cdp, depVars);

    // If this node is commutative, consider the commuted order.
    boolean isCommutativeIntrinsic = node.isCommutativeIntrinsic(cdp);
    if (nodeInfo.hasProperty(SDNPCommutative) || isCommutativeIntrinsic) {
      Util.assertion((node.getNumChildren() == 2 || isCommutativeIntrinsic), "Commutative but does't not have 2 chidren!");


      int nc = 0;
      for (int i = 0, e = node.getNumChildren(); i != e; i++) {
        TreePatternNode child = node.getChild(i);
        if (child.isLeaf()) {
          DefInit di = child.getLeafValue() instanceof DefInit ? (DefInit) child.getLeafValue() : null;
          if (di != null) {
            Record rr = di.getDef();
            if (rr.isSubClassOf("Register"))
              continue;
          }
        }
        ++nc;
      }

      if (isCommutativeIntrinsic) {
        Util.assertion(nc >= 3, "Commutative intrinsic should have at least 3 children!");
        ArrayList<ArrayList<TreePatternNode>> variants = new ArrayList<>();
        variants.add(childVariants.get(0));
        variants.add(childVariants.get(2));
        variants.add(childVariants.get(1));

        for (int i = 3; i != nc; i++)
          variants.add(childVariants.get(i));
        combineChildVariants(node, variants, outVariants, cdp, depVars);
      } else if (nc == 2) {
        combineChildVariants(node, childVariants.get(1), childVariants.get(0),
            outVariants, cdp, depVars);
      }
    }
  }

  private static void combineChildVariants(TreePatternNode orig,
                                           ArrayList<TreePatternNode> lhs,
                                           ArrayList<TreePatternNode> rhs,
                                           ArrayList<TreePatternNode> outVars,
                                           CodeGenDAGPatterns cdp,
                                           HashSet<String> depVars) {
    ArrayList<ArrayList<TreePatternNode>> childVariants = new ArrayList<>();
    childVariants.add(lhs);
    childVariants.add(rhs);
    combineChildVariants(orig, childVariants, outVars, cdp, depVars);
  }

  /**
   * Given a bunch of permutations of each child of the
   * 'operator' node, put them together in all possible ways.
   *
   * @param orig
   * @param childVariants
   * @param outVariants
   * @param cdp
   * @param depVars
   */
  private static void combineChildVariants(TreePatternNode orig,
                                           ArrayList<ArrayList<TreePatternNode>> childVariants,
                                           ArrayList<TreePatternNode> outVariants,
                                           CodeGenDAGPatterns cdp,
                                           HashSet<String> depVars) {
    for (int i = 0, e = childVariants.size(); i != e; i++) {
      if (childVariants.get(i).isEmpty())
        return;
    }

    TIntArrayList idxs = new TIntArrayList();
    idxs.fill(0, childVariants.size(), 0);
    boolean notDone;
    do {
      if (TableGen.DEBUG && !idxs.isEmpty()) {
        System.err.printf("%s: Idxs = [ ", orig.getOperator().getName());
        for (int i = 0, e = idxs.size(); i != e; i++)
          System.err.printf("%d ", idxs.get(i));

        System.err.println("]");
      }

      ArrayList<TreePatternNode> newChildren = new ArrayList<>();
      for (int i = 0, e = childVariants.size(); i != e; i++)
        newChildren.add(childVariants.get(i).get(idxs.get(i)));

      TreePatternNode r = new TreePatternNode(orig.getOperator(), newChildren, orig.getNumTypes());

      r.setName(orig.getName());
      r.setPredicateFns(orig.getPredicateFns());
      r.setTransformFn(orig.getTransformFn());
      r.setTypes(orig.getExtTypes());

      StringBuilder errString = new StringBuilder();
      if (r.canPatternMatch(errString, cdp)) {
        boolean alreadyExists = false;
        for (int i = 0, e = outVariants.size(); i != e; i++) {
          if (r.isIsomorphicTo(outVariants.get(i), depVars)) {
            alreadyExists = true;
            break;
          }
        }

        if (!alreadyExists)
          outVariants.add(r);
      }

      // Increment indices to the next permutation by incrementing the
      // indicies from last index backward, e.g., generate the sequence
      // [0, 0], [0, 1], [1, 0], [1, 1].
      int j;
      for (j = idxs.size() - 1; j >= 0; --j) {
        int tmp = idxs.get(j);
        idxs.set(j, tmp + 1);
        if (tmp + 1 == childVariants.get(j).size())
          idxs.set(j, 0);
        else
          break;
      }
      notDone = j >= 0;
    } while (notDone);
  }

  private static void gatherChildrenOfAssociativeOpcode(
      TreePatternNode node,
      ArrayList<TreePatternNode> children) {
    Util.assertion(node.getNumChildren() == 2, "Assocative but doesn't have 2 children");
    Record operator = node.getOperator();

    if (!node.getName().isEmpty() || !node.getPredicateFns().isEmpty()
        || node.getTransformFn() != null) {
      children.add(node);
      return;
    }

    if (node.getChild(0).isLeaf() || !node.getChild(0).getOperator().equals(operator))
      children.add(node.getChild(0));
    else
      gatherChildrenOfAssociativeOpcode(node.getChild(0), children);

    if (node.getChild(1).isLeaf() || !node.getChild(1).getOperator().equals(operator))
      children.add(node.getChild(1));
    else
      gatherChildrenOfAssociativeOpcode(node.getChild(1), children);
  }

  private void findDepVarsOf(TreePatternNode node, TObjectIntHashMap<String> depMap) {
    if (node.isLeaf()) {
      if (node.getLeafValue() instanceof DefInit) {
        if (depMap.containsKey(node.getName()))
          depMap.put(node.getName(), depMap.get(node.getName()) + 1);
        else
          depMap.put(node.getName(), 1);
      }
    } else {
      for (int i = 0, e = node.getNumChildren(); i != e; i++)
        findDepVarsOf(node.getChild(i), depMap);
    }
  }

  /**
   * Find dependent variables within child patterns.
   *
   * @param node
   * @param depVars
   */
  private void findDepVars(TreePatternNode node, HashSet<String> depVars) {
    TObjectIntHashMap<String> depCounts = new TObjectIntHashMap<>();
    findDepVarsOf(node, depCounts);
    depCounts.forEachEntry((o, i) -> {
      if (i > 1)
        depVars.add(o);
      return true;
    });
  }

  private static void dumpDepVars(Set<String> depVars) {
    if (depVars.isEmpty()) {
      System.out.printf("<empty set>");
    } else {
      System.out.printf("[ ");
      for (String depVar : depVars) {
        System.out.printf("%s ", depVar);
      }
      System.out.printf("]");
    }
  }

  private void parsePatterns() {
    ArrayList<Record> patterns = records.getAllDerivedDefinition("Pattern");

    for (int i = 0, e = patterns.size(); i != e; ++i) {
      DagInit tree = patterns.get(i).getValueAsDag("PatternToMatch");
      DefInit opDef = (DefInit) tree.getOperator();
      Record operator = opDef.getDef();

      TreePattern pattern;
      if (!operator.getName().equals("parallel"))
        pattern = new TreePattern(patterns.get(i), tree, true, this);
      else {
        ArrayList<Init> values = new ArrayList<>();
        RecTy listTy = null;
        for (int j = 0, ee = tree.getNumArgs(); j != ee; ++j) {
          values.add(tree.getArg(j));
          TypedInit targ = tree.getArg(j) instanceof TypedInit ? (TypedInit) tree.getArg(j) : null;
          if (targ == null) {
            System.err.printf("In dag: %s", tree.toString());
            System.err.print(" -- Untyped argument in pattern\n");
            Util.assertion("Untyped argument in pattern");
          }
          if (listTy != null) {
            listTy = resolveTypes(listTy, targ.getType());
            if (listTy == null) {
              System.err.printf("In dag: %s", tree.toString());
              System.err.print(" -- Incompatible types in pattern argument\n");
              Util.assertion("Incompatible types in pattern arguments");
            }
          } else {
            listTy = targ.getType();
          }
        }

        ListInit li = new ListInit(values, new RecTy.ListRecTy(listTy));
        pattern = new TreePattern(patterns.get(i), li, true, this);
      }

      ListInit li = patterns.get(i).getValueAsListInit("ResultInstrs");
      if (li == null || li.getSize() == 0) continue;

      // Parse the instruction.
      TreePattern result = new TreePattern(patterns.get(i), li, false, this);
      if (result.getNumTrees() != 1) {
        result.error("Cannot handle instructions producing instructions " +
            "with temporaries yet!");
      }
      TreeMap<String, TreePatternNode> instInputs = new TreeMap<>();
      TreeMap<String, TreePatternNode> instResults = new TreeMap<>();
      ArrayList<Record> instImpInputs = new ArrayList<>();
      ArrayList<Record> instImpResults = new ArrayList<>();
      for (int j = 0, ee = pattern.getNumTrees(); j != ee; ++j) {
        findPatternInputsAndOutputs(pattern, pattern.getTree(j), instInputs, instResults,
            instImpInputs, instImpResults);
      }

      parseOnePattern(patterns.get(i), pattern, result, instImpResults);
    }
  }

  private ArrayList<utils.tablegen.Predicate> makePredList(ListInit l) {
    ArrayList<utils.tablegen.Predicate> res = new ArrayList<>();
    for (int i = 0, e = l.getSize(); i < e; i++) {
      Init ii = l.getElement(i);
      if (ii instanceof DefInit)
        res.add(new utils.tablegen.Predicate(((DefInit) ii).getDef(), true));
      else
        Util.shouldNotReachHere("Non-def on the list!");
    }
    Collections.sort(res);
    return res;
  }

  private void parseOnePattern(Record theDef, TreePattern pattern, TreePattern result,
                               ArrayList<Record> instImpResults) {

    // Inline pattern fragments into it.
    pattern.inlinePatternFragments();
    // Inline pattern fragments into it.
    result.inlinePatternFragments();

    if (result.getNumTrees() != 1) {
      result.error("Cannot handle instructions producing instructions " +
          "with temporaries yet!");
    }

    boolean iterateInference;
    boolean inferredAllPatternTypes, inferredAllResultTypes;
    do {
      // Infer as many types as possible.  If we cannot infer all of them, we
      // can never do anything with this pattern: report it to the user.
      inferredAllPatternTypes = pattern.inferAllTypes(pattern.getNamedNodes());

      // Infer as many types as possible.  If we cannot infer all of them, we
      // can never do anything with this pattern: report it to the user.
      inferredAllResultTypes = result.inferAllTypes(pattern.getNamedNodes());

      iterateInference = false;
      // Apply the type of the result to the source pattern.  This helps us
      // resolve cases where the input type is known to be a pointer type (which
      // is considered resolved), but the result knows it needs to be 32- or
      // 64-bits.  Infer the other way for good measure.
      for (int k = 0, sz = Math.min(pattern.getTree(0).getNumTypes(),
          result.getTree(0).getNumTypes()); k < sz; k++) {
        iterateInference = pattern.getTree(0).
            updateNodeType(k, result.getTree(0).getExtType(k), result);
        iterateInference |= result.getTree(0).
            updateNodeType(k, pattern.getTree(0).getExtType(k), result);
      }

      // If our iteration has converged and the input pattern's types are fully
      // resolved but the result pattern is not fully resolved, we may have a
      // situation where we have two instructions in the result pattern and
      // the instructions require a common register class, but don't care about
      // what actual MVT is used.  This is actually a bug in our modelling:
      // output patterns should have register classes, not MVTs.
      //
      // In any case, to handle this, we just go through and disambiguate some
      // arbitrary types to the result pattern's nodes.

      // for example:
      // Pattern: (scalar_to_vector:{ *:[v16i8]} GPR:{ *:[i32]}:$src)
      // Result:(INSERT_SUBREG:{ *:[v16i8]} (IMPLICIT_DEF:{ *:[v16i8]}), (VSETLNi8:{ *:[f64, v8i8, v4i16, v2i32, v1i64, v2f32]} (IMPLICIT_DEF:{ *:[v8i8]}), GPR:{ *:[i32]}:$src, 0:{ *:[i32]}), dsub_0:{ *:[i32]})
      if (!iterateInference && inferredAllPatternTypes && !inferredAllResultTypes) {
        iterateInference = forceArbitraryInstResultType(result.getTree(0), result);
      }
    } while (iterateInference);

    if (!inferredAllPatternTypes)
      pattern.error("Could not infer all types in pattern!");
    if (!inferredAllResultTypes)
      pattern.error("Could not infer all types in pattern result!");

    // Promote the xform function to be an explicit node if set.
    TreePatternNode dstPattern = result.getOnlyTree();
    ArrayList<TreePatternNode> resultNodeOperands = new ArrayList<>();
    for (int ii = 0, ee = dstPattern.getNumChildren(); ii != ee; ++ii) {
      TreePatternNode opNode = dstPattern.getChild(ii);
      Record xform = opNode.getTransformFn();
      if (xform != null) {
        opNode.setTransformFn(null);
        ArrayList<TreePatternNode> children = new ArrayList<>();
        children.add(opNode);
        opNode = new TreePatternNode(xform, children, opNode.getNumTypes());
      }
      resultNodeOperands.add(opNode);
    }

    dstPattern = result.getOnlyTree();
    if (!dstPattern.isLeaf())
      dstPattern = new TreePatternNode(dstPattern.getOperator(),
          resultNodeOperands, dstPattern.getNumTypes());

    dstPattern.setTypes(result.getOnlyTree().getExtTypes());
    TreePattern temp = new TreePattern(result.getRecord(), dstPattern, false, this);
    temp.inferAllTypes();

    StringBuilder reason = new StringBuilder();
    if (!pattern.getTree(0).canPatternMatch(reason, this))
      pattern.error("Pattern can never match: " + reason.toString());

    addPatternsToMatch(new PatternToMatch(
        theDef,
        makePredList(theDef.getValueAsListInit("Predicates")),
        pattern.getTree(0),
        temp.getOnlyTree(), instImpResults,
        (int) theDef.getValueAsInt("AddedComplexity")));
  }

  /**
   * Given a pattern result with an unresolved type, see if we can find one
   * instruction with an unresolved result type.  Force this result type to an
   * arbitrary element if it's possible types to converge results.
   * @param n
   * @param tp
   * @return
   */
  private static boolean forceArbitraryInstResultType(TreePatternNode n, TreePattern tp) {
    if (n.isLeaf()) return false;

    // Analyze children.
    for (int i = 0, e = n.getNumChildren(); i < e; ++i)
      if (forceArbitraryInstResultType(n.getChild(i), tp))
        return true;

    if (!n.getOperator().isSubClassOf("Instruction"))
      return false;

    // If this type is already concrete or completely unknown, we can't do anything.
    TypeInfer infer = tp.getTypeInfer();
    for (int i = 0, e = n.getNumTypes(); i < e; ++i) {
      if (n.getExtType(i).isEmpty() || infer.isConcrete(n.getExtType(i), false))
        continue;

      // Otherwise, force its type to an arbitrary choice.
      if (infer.forceArbitrary(n.getExtType(0)))
        return true;
    }
    return false;
  }

  /**
   * Scan the specified TreePatternNode (which is
   * part of "I", the instruction), computing the set of inputs and outputs of
   * the pattern.  Report errors if we see anything naughty.
   *
   * @param inst
   * @param pat
   * @param instInputs
   * @param instResults
   * @param instImpInputs
   * @param instImpResults
   */
  private void findPatternInputsAndOutputs(TreePattern inst,
                                           TreePatternNode pat,
                                           TreeMap<String, TreePatternNode> instInputs,
                                           TreeMap<String, TreePatternNode> instResults,
                                           ArrayList<Record> instImpInputs,
                                           ArrayList<Record> instImpResults) {
    // If the instruction pattern still has unresolved fragments. For "named"
    // nodes we must resolve those here.
    if (!pat.getName().isEmpty()) {
      TreePattern srcPat = new TreePattern(inst.getRecord(), pat, true, this);
      srcPat.inlinePatternFragments();
      srcPat.inferAllTypes();
      pat = srcPat.getOnlyTree();
    }

    if (pat.isLeaf()) {
      boolean isUse = handleUse(inst, pat, instInputs, instImpInputs);
      if (!isUse && pat.getTransformFn() != null)
        inst.error("Cannot specify a transform function for a non-input value!");
      return;
    } else if (pat.getOperator().getName().equals("implicit")) {
      for (int i = 0, e = pat.getNumChildren(); i != e; i++) {
        TreePatternNode dest = pat.getChild(i);
        if (!dest.isLeaf())
          inst.error("implicitly defined value should be a register!");

        DefInit val = dest.getLeafValue() instanceof DefInit ? (DefInit) dest.getLeafValue() : null;
        if (val == null || !val.getDef().isSubClassOf("Register"))
          inst.error("implicitly defined value should be a register!");

        instImpResults.add(val.getDef());
      }
      return;
    } else if (!pat.getOperator().getName().equals("set")) {
      for (int i = 0, e = pat.getNumChildren(); i != e; i++) {
        TreePatternNode child = pat.getChild(i);
        if (child.getNumTypes() <= 0) {
          inst.error("Cannot have void nodes inside of patterns!");
        }
        findPatternInputsAndOutputs(inst, child, instInputs,
            instResults, instImpInputs, instImpResults);
      }

      boolean isUse = handleUse(inst, pat, instInputs, instImpInputs);

      if (!isUse && pat.getTransformFn() != null)
        inst.error("Cannot specify a transform function for a non-input value!");
      return;
    }

    // Otherwise, this is a set, validate and collect instruction results.
    if (pat.getNumChildren() == 0)
      inst.error("set requires operands!");

    if (pat.getTransformFn() != null)
      inst.error("Cannot specify a transform function on a set node!");

    int numDests = pat.getNumChildren() - 1;
    for (int i = 0; i != numDests; ++i) {
      TreePatternNode dest = pat.getChild(i);
      if (!dest.isLeaf())
        inst.error("set destination should be a register!");

      DefInit val = dest.getLeafValue() instanceof DefInit ? (DefInit) dest.getLeafValue() : null;
      if (val == null)
        inst.error("set destination should be a register!");

      if (val.getDef().isSubClassOf("RegisterClass") ||
          val.getDef().isSubClassOf("PointerLikeRegClass")) {
        if (dest.getName().isEmpty())
          inst.error("set destination must have a namespace!");
        if (instResults.containsKey(dest.getName()))
          inst.error("can not set '" + dest.getName() + "' multiple times");
        instResults.put(dest.getName(), dest);
      } else if (val.getDef().isSubClassOf("Register")) {
        instImpResults.add(val.getDef());
      } else {
        inst.error("set destination should be a register!");
      }
    }

    // Verify and collect info from the computation.
    findPatternInputsAndOutputs(inst, pat.getChild(numDests),
        instInputs, instResults, instImpInputs, instImpResults);
  }

  /**
   * Given "tree" a leaf in the pattern, check to see if it is an
   * instruction input.  Return true if this is a real use.
   *
   * @param pattern
   * @param tree
   * @param instInputs
   * @param instImpInputs
   * @return
   */
  private static boolean handleUse(TreePattern pattern,
                                   TreePatternNode tree,
                                   TreeMap<String, TreePatternNode> instInputs,
                                   ArrayList<Record> instImpInputs) {
    if (tree.getName().isEmpty()) {
      if (tree.isLeaf()) {
        DefInit di = (tree.getLeafValue() instanceof DefInit) ? (DefInit) tree.getLeafValue() : null;
        if (di != null && di.getDef().isSubClassOf("RegisterClass"))
          pattern.error("Input " + di.getDef().getName() + " must be named!");
        else if (di != null && di.getDef().isSubClassOf("Register"))
          instImpInputs.add(di.getDef());
      }
      return false;
    }

    Record rec;
    if (tree.isLeaf()) {
      DefInit di = (tree.getLeafValue() instanceof DefInit) ? (DefInit) tree.getLeafValue() : null;
      if (di == null)
        pattern.error("Input $" + tree.getName() + " must be an identifier!");
      rec = di.getDef();
    } else
      rec = tree.getOperator();

    if (rec.getName().equals("srcvalue"))
      return false;

    if (!instInputs.containsKey(tree.getName()))
      instInputs.put(tree.getName(), tree);
    else {
      TreePatternNode slot = instInputs.get(tree.getName());
      Record slotRec;
      if (slot.isLeaf())
        slotRec = (slot.getLeafValue() instanceof DefInit) ? ((DefInit) slot.getLeafValue()).getDef() : null;
      else {
        Util.assertion(slot.getNumChildren() == 0, "can't be a use with children!");
        slotRec = slot.getOperator();
      }

      if (!rec.equals(slotRec))
        pattern.error("All $" + tree.getName() + " inputs must agree with each other");
      if (!slot.getExtTypes().equals(tree.getExtTypes()))
        pattern.error("All $" + tree.getName() + " inputs must agree with each other");
    }

    return true;
  }

  /**
   * Parse all of the instructions, inlining and resolving
   * any fragments involved.  This populates the Instructions list with fully
   * resolved instructions.
   */
  private void parseInstructions() {
    ArrayList<Record> instrs = records.getAllDerivedDefinition("Instruction");
    for (int idx = 0; idx < instrs.size(); idx++) {
      Record instr = instrs.get(idx);
      ListInit li = null;

      if (instr.getValueInit("Pattern") instanceof ListInit)
        li = instr.getValueAsListInit("Pattern");

      // If there is no pattern, only collect minimal information about the
      // instruction for its operand list.  We have to assume that there is one
      // result, as we have no detailed info.
      if (li == null || li.getSize() == 0) {
        ArrayList<Record> results = new ArrayList<>();
        ArrayList<Record> operands = new ArrayList<>();

        CodeGenInstruction instrInfo = target.getInstruction(instr.getName());

        if (!instrInfo.operandList.isEmpty()) {
          // These produce no results
          if (instrInfo.numDefs == 0) {
            instrInfo.operandList.forEach(op -> operands.add(op.rec));
          } else {
            // Assume the first operand is the result.
            results.add(instrInfo.operandList.get(0).rec);

            // The rest are inputs.
            for (int i = 1, e = instrInfo.operandList.size(); i != e; i++)
              operands.add(instrInfo.operandList.get(i).rec);
          }
        }

        // create and insert the instruction.
        ArrayList<Record> impResults = new ArrayList<>();
        ArrayList<Record> impOperands = new ArrayList<>();
        instructions.put(instr, new DAGInstruction(results, operands, impResults, impOperands));
        continue;   // no pattern.
      }

      CodeGenInstruction cgi = target.getInstruction(instr.getName());
      parseInstructionPattern(cgi, li, instructions);
    }

    for (Map.Entry<Record, DAGInstruction> pair : instructions.entrySet()) {
      Record instr = pair.getKey();
      DAGInstruction theInst = pair.getValue();
      TreePattern tp = theInst.getPattern();
      // no pattern.
      if (tp == null) continue;

      TreePatternNode pattern = tp.getTree(0);
      TreePatternNode srcPattern;
      if (pattern.getOperator().getName().equals("set"))
        srcPattern = pattern.getChild(pattern.getNumChildren()-1).clone();
      else
        srcPattern = pattern;

      addPatternsToMatch(new PatternToMatch(
          instr,
          makePredList(instr.getValueAsListInit("Predicates")),
          srcPattern,
          theInst.getResultPattern(),
          theInst.getImpResults(),
          (int)instr.getValueAsInt("AddedComplexity")));
    }
  }

  /**
   * Parse the pattern for each instruction and store the result into {@code dagInstrs}.
   *
   * @param cgi
   * @param pat
   * @param dagInstrs
   */
  private void parseInstructionPattern(CodeGenInstruction cgi, ListInit pat,
                                       TreeMap<Record, DAGInstruction> dagInstrs) {
    Util.assertion(!dagInstrs.containsKey(cgi.theDef), "Instruction has been parsed");
    TreePattern tp = new TreePattern(cgi.theDef, pat, true, this);
    tp.inlinePatternFragments();
    if (!tp.inferAllTypes())
      tp.error("Could not infer all types in pattern!");

    TreeMap<String, TreePatternNode> instInputs = new TreeMap<>();
    TreeMap<String, TreePatternNode> instResults = new TreeMap<>();
    ArrayList<Record> instImpResults = new ArrayList<>();
    ArrayList<Record> instImpInputs = new ArrayList<>();

    // Verify that the top-level forms in the instruction are of void type, and
    // fill in the InstResults map.
    // FIXME  [(set VR128:$dst, (v4f32 (shufp:$src3 VR128:$src1, VR128:$src2)))]
    for (int j = 0, e = tp.getNumTrees(); j != e; ++j) {
      TreePatternNode child = tp.getTree(j);
      StringBuilder buf = new StringBuilder();
      if (child.getNumTypes() > 0) {
        for (int k = 0, sz = child.getNumTypes(); k < sz; k++) {
          if (k > 0)
            buf.append(',');
          buf.append(child.getExtType(k).toString());
        }
        tp.error("Top-level forms in instruction pattern should have" +
            " void types, has types " + buf.toString());
      }

      // Find inputs and outputs, and verify the structure of the uses/defs.
      findPatternInputsAndOutputs(tp, child, instInputs, instResults,
          instImpInputs, instImpResults);
    }

    // Now that we have inputs and outputs of the pattern, inspect the operands
    // list for the instruction.  This determines the order that operands are
    // added to the machine instruction the node corresponds to.
    int numResults = instResults.size();

    // Parse the operands list from the (ops) list, validating it.
    Util.assertion(tp.getArgList().isEmpty(), "Args list should still be empty here!");

    // Check that all of the results occur first in the list.
    ArrayList<Record> results = new ArrayList<>();
    ArrayList<TreePatternNode> resNodes = new ArrayList<>();
    for (int j = 0; j != numResults; j++) {
      if (j == cgi.operandList.size())
        tp.error("'" + instResults.entrySet().iterator().next().getKey()
            + "' set but does not appear in operand list!");

      String opName = cgi.operandList.get(j).name;

      // Check that it exists in InstResults.
      if (!instResults.containsKey(opName))
        tp.error("Operand $" + opName + " does not exist in operand list!");
      TreePatternNode rnode = instResults.get(opName);

      resNodes.add(rnode);
      Record r = rnode.getLeafValue() instanceof DefInit ?
          ((DefInit) rnode.getLeafValue()).getDef() : null;
      if (r == null)
        tp.error("Operand $" + opName + " should be a set destination: all "
            + "outputs must occur before inputs in operand list!");

      if (cgi.operandList.get(j).rec != r) {
        tp.error("Operand $" + opName + " class mismatch!");
      }

      // Remember the return type.
      results.add(cgi.operandList.get(j).rec);

      // Okay, this one checks out.
      instResults.remove(opName);
    }

    // Loop over the inputs next.  Make a copy of InstInputs so we can destroy
    // the copy while we're checking the inputs.
    TreeMap<String, TreePatternNode> instInputsCheck = new TreeMap<>(instInputs);

    ArrayList<TreePatternNode> resultNodeOperands = new ArrayList<>();
    ArrayList<Record> operands = new ArrayList<>();
    for (int j = numResults, e = cgi.operandList.size(); j != e; j++) {
      OperandInfo op = cgi.operandList.get(j);
      String opName = op.name;
      if (opName.isEmpty())
        tp.error("Operand #" + j + " in operands list has no namespace!");

      if (!instInputsCheck.containsKey(opName) ||
          instInputsCheck.get(opName) == null) {
        // If this is an predicate operand or optional def operand with an
        // DefaultOps set filled in, we can ignore this.  When we codegen it,
        // we will do so as always executed.
        if (op.rec.isSubClassOf("PredicateOperand") ||
            op.rec.isSubClassOf("OptionalDefOperand")) {
          // Does it have a non-empty DefaultOps field?  If so, ignore this
          // operand.
          if (!getDefaultOperand(op.rec).defaultOps.isEmpty())
            continue;
        }
        tp.error("Operand $" + opName + " does not appear in the instruction pattern");
        System.exit(1);
      }

      TreePatternNode inVal = instInputsCheck.get(opName);
      instInputsCheck.remove(opName);

      if (inVal.isLeaf() && inVal.getLeafValue() instanceof DefInit) {
        Record inRec = ((DefInit) inVal.getLeafValue()).getDef();
        if (op.rec != inRec && !inRec.isSubClassOf("ComplexPattern"))
          tp.error("Operand $" + opName + "'s register class disagrees" +
              " between the operand and pattern");
      }
      operands.add(op.rec);

      // Construct the result for the dest-pattern operand list.
      TreePatternNode opNode = inVal.clone();

      // No predicate is useful on the result.
      opNode.clearPredicateFns();

      // Promote the xform function to be an explicit node if set.
      Record xform = opNode.getTransformFn();
      if (xform != null) {
        opNode.setTransformFn(null);
        ArrayList<TreePatternNode> childs = new ArrayList<>();
        childs.add(opNode);
        opNode = new TreePatternNode(xform, childs, opNode.getNumTypes());
      }
      resultNodeOperands.add(opNode);
    }

    if (!instInputsCheck.isEmpty())
      tp.error("Input operand $" + instInputsCheck.entrySet().iterator().next().getKey() +
          " occurs in pattern but not in operands list!");

    TreePatternNode resultPattern = new TreePatternNode(tp.getRecord(), resultNodeOperands,
        getNumNodeResults(tp.getRecord(), this));

    // Copy fully inferred output node type to instruction result pattern.
    for (int i = 0; i < numResults; i++) {
      Util.assertion(resNodes.get(i).getNumTypes() == 1, "FIXME: Unhandled!");
      resultPattern.setType(i, resNodes.get(i).getExtType(0));
    }

    // Create and insert the instruction.
    Record r = tp.getRecord();
    DAGInstruction theInst = new DAGInstruction(results, operands,
        instImpResults, instImpInputs, tp);
    dagInstrs.put(r, theInst);

    // Use a temporary tree pattern to infer all types and make sure that
    // the constructed result is correct. This depends on the instruction
    // already beging inserted into the instructions map.
    TreePattern temp = new TreePattern(r, resultPattern, false, this);
    temp.inferAllTypes(tp.getNamedNodes());
    theInst.setResultPattern(temp.getOnlyTree());

    if (TableGen.DEBUG)
      tp.dump();
  }

  private void parseDefaultOperands() {
    ArrayList<Record>[] defaultOps = new ArrayList[2];

    defaultOps[0] = records.getAllDerivedDefinition("PredicateOperand");
    defaultOps[1] = records.getAllDerivedDefinition("OptionalDefOperand");

    // Find some SDNode.
    Util.assertion(!sdnodes.isEmpty(), "No SDNodes parsed?");
    Init someSDNode = new DefInit(sdnodes.entrySet().iterator().next().getKey());

    for (int itr = 0; itr != 2; itr++) {
      for (int i = 0, e = defaultOps[itr].size(); i != e; i++) {
        DagInit defaultInfo = defaultOps[itr].get(i).getValueAsDag("DefaultOps");

        ArrayList<Pair<Init, String>> ops = new ArrayList<>();
        for (int op = 0, ee = defaultInfo.getNumArgs(); op != ee; ++op) {
          ops.add(Pair.get(defaultInfo.getArg(op), defaultInfo.getArgName(op)));
        }

        DagInit di = new DagInit(someSDNode, "", ops);

        TreePattern pattern = new TreePattern(defaultOps[itr].get(i), di, false, this);
        Util.assertion(pattern.getNumTrees() == 1, "This ctor can only produce one tree!");

        DAGDefaultOperand defaultOpInfo = new DAGDefaultOperand();

        TreePatternNode t = pattern.getTree(0);
        for (int op = 0, sz = t.getNumChildren(); op != sz; ++op) {
          TreePatternNode node = t.getChild(op);
          // Resolve all types.
          while (node.applyTypeConstraints(pattern, false)) ;

          if (node.containsUnresolvedType(pattern)) {
            if (itr == 0) {
              Error.printFatalError("Value #" + i + " of PredicateOperand '"
                  + defaultOps[itr].get(i).getName() + "' doesn't have concrete type!");
            } else {
              Error.printFatalError("Value #" + i + " of OptionalDefOperand '"
                  + defaultOps[itr].get(i).getName() + "' doesn't have concrete type!");
            }
          }
          defaultOpInfo.defaultOps.add(node);
        }
        defaultOperands.put(defaultOps[itr].get(i), defaultOpInfo);
      }
    }
  }

  private void parsePatternFragments() {
    ArrayList<Record> fragments = records.getAllDerivedDefinition("PatFrag");

    // Step#1. parse all of the fragments.
    for (int i = 0, e = fragments.size(); i != e; i++) {
      Record patFrag = fragments.get(i);
      DagInit tree = patFrag.getValueAsDag("Fragment");
      TreePattern tp = new TreePattern(patFrag, tree, true, this);
      patternFragments.put(patFrag, tp);

      ArrayList<String> args = tp.getArgList();
      HashSet<String> operandsSet = new HashSet<>(args);

      if (operandsSet.contains(""))
        tp.error("Cannot have unnamed 'node' values in pattern fragment!");

      DagInit opsList = patFrag.getValueAsDag("Operands");
      DefInit opsOp = opsList.getOperator() instanceof DefInit ?
          (DefInit) opsList.getOperator() : null;
      if (opsOp == null || (!opsOp.getDef().getName().equals("ops")
          && !opsOp.getDef().getName().equals("outs") && !opsOp.getDef().getName().equals("ins"))) {
        tp.error("Operands list should start with '(ops ...'!");
      }

      // Copy over the arguments.
      args.clear();

      for (int j = 0, sz = opsList.getNumArgs(); j != sz; ++j) {
        if (!(opsList.getArg(j) instanceof DefInit)
            || !((DefInit) opsList.getArg(j)).getDef().getName().equals("node")) {
          tp.error("Operands list should all be 'node' values.");
        }

        if (opsList.getArgName(j).isEmpty())
          tp.error("Operands list should have names for each operand!");
        if (!operandsSet.contains(opsList.getArgName(j))) {
          tp.error("'" + opsList.getArgName(j)
              + "' does not occur in pattern or was multiply specified!");
        }
        operandsSet.remove(opsList.getArgName(j));
        args.add(opsList.getArgName(j));
      }

      if (!operandsSet.isEmpty()) {
        tp.error("Operands list nodes not contain an entry for operand '"
            + operandsSet.iterator().next() + "'!");
      }

      // If there is a code init for this fragment, keep track of the fact that
      // this fragment uses it.
      TreePredicateFn predFn = new TreePredicateFn(tp);
      if (!predFn.isAlwaysTrue()) {
        tp.getOnlyTree().addPredicateFn(predFn);
      }

      Record transform = patFrag.getValueAsDef("OperandTransform");
      if (!getSDNodeTransform(transform).second.isEmpty())
        tp.getOnlyTree().setTransformFn(transform);
    }

    // Now that we've parsed all of the tree fragments, do a closure on them so
    // that there are not references to PatFrags left inside of them.

    // For example, when we encounter following PatFrag, a TreePattern object will be
    // constructed with operator zextload and one child node, predicate function.
    // def zextloadi1  : PatFrag<(ops node:$ptr), (zextload node:$ptr), [{
    //   return ((LoadSDNode)n).getMemoryVT().getSimpleVT().simpleVT == MVT.i1;
    // }]>;
    //
    // Note that, there is a reference to other PatFrag (zextload) in this TreePattern
    //, so we need following code snip to accomplish this function.
    for (int i = 0, e = fragments.size(); i != e; i++) {
      TreePattern pat = patternFragments.get(fragments.get(i));
      pat.inlinePatternFragments();

      // Infer as many types as possible.  Don't worry about it if we don't infer
      // all of them, some may depend on the inputs of the pattern.
      pat.inferAllTypes();

      // If debugging, print out the pattern fragment result.
      if (TableGen.DEBUG) {
        System.err.println(i);
        pat.dump();
        System.err.println();
      }
    }
  }

  private void parseComplexPatterns() {
    ArrayList<Record> ams = records.getAllDerivedDefinition("ComplexPattern");
    for (Record r : ams) {
      complexPatterns.put(r, new ComplexPattern(r));
    }
  }

  private void parseNodeTransforms() {
    ArrayList<Record> xforms = records.getAllDerivedDefinition("SDNodeXForm");
    for (Record r : xforms) {
      Record sdNode = r.getValueAsDef("Opcode");
      String code = r.getValueAsCode("XFormFunction");
      sdNodeXForms.put(r, new Pair<>(sdNode, code));
    }
  }

  /**
   * Parse all of the SDNode definitions for the target, populating SDNodes.
   */
  private void parseNodeInfo() {
    ArrayList<Record> nodes = records.getAllDerivedDefinition("SDNode");
    for (Record node : nodes) {
      sdnodes.put(node, new SDNodeInfo(node, target.getHwModes()));
    }

    intrinsicVoidSDNode = getSDNodeNamed("intrinsic_void");
    intrinsicWChainSDNode = getSDNodeNamed("intrinsic_w_chain");
    intrinsicWOChainSDNode = getSDNodeNamed("intrinsic_wo_chain");
  }


  /**
   * Read all of the intrinsics defined in the specified .td file.
   *
   * @param rc
   * @param targetOnly
   * @return
   */
  public ArrayList<CodeGenIntrinsic> loadIntrinsics(RecordKeeper rc, boolean targetOnly) {
    try {
      ArrayList<Record> i = rc.getAllDerivedDefinition("Intrinsic");

      ArrayList<CodeGenIntrinsic> res = new ArrayList<>();

      for (Record intr : i) {
        if (intr.getValueAsBit("isTarget") == targetOnly)
          res.add(new CodeGenIntrinsic(intr));
      }
      return res;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public CodeGenTarget getTarget() {
    return target;
  }

  public Record getSDNodeNamed(String name) {
    Record rec = records.getDef(name);
    if (rec == null || !rec.isSubClassOf("SDNode")) {
      System.err.printf("Error getting SDNode '%s'!\n", name);
      System.exit(1);
    }
    return rec;
  }

  public SDNodeInfo getSDNodeInfo(Record r) {
    Util.assertion(sdnodes.containsKey(r), "Undefined node!");
    return sdnodes.get(r);
  }

  public Pair<Record, String> getSDNodeTransform(Record r) {
    Util.assertion(sdNodeXForms.containsKey(r), "Invalid transform");
    return sdNodeXForms.get(r);
  }

  public TreeMap<Record, Pair<Record, String>> getSdNodeXForms() {
    return sdNodeXForms;
  }

  public ComplexPattern getComplexPattern(Record r) {
    Util.assertion(complexPatterns.containsKey(r), "Unknown address mode!");
    return complexPatterns.get(r);
  }

  public CodeGenIntrinsic getIntrinsic(Record r) {
    for (CodeGenIntrinsic cgi : intrinsics)
      if (cgi.theDef.equals(r)) return cgi;
    for (CodeGenIntrinsic cgi : tgtIntrinsics)
      if (cgi.theDef.equals(r)) return cgi;

    Util.assertion("Undefined intrinsic!");
    return null;
  }

  public CodeGenIntrinsic getIntrinsicInfo(int iid) {
    if (iid - 1 < intrinsics.size())
      return intrinsics.get(iid - 1);
    if (iid - intrinsics.size() - 1 < tgtIntrinsics.size())
      return tgtIntrinsics.get(iid - intrinsics.size() - 1);
    Util.assertion("Bad intrinsic ID!");
    return null;
  }

  public int getIntrinsicID(Record r) {
    int idx = 0;
    for (CodeGenIntrinsic cgi : intrinsics) {
      if (cgi.theDef.equals(r))
        return idx;
      ++idx;
    }
    idx = 0;
    for (CodeGenIntrinsic cgi : tgtIntrinsics) {
      if (cgi.theDef.equals(r))
        return idx;
      ++idx;
    }
    Util.assertion("Undefined intrinsic!");
    return -1;
  }

  public DAGDefaultOperand getDefaultOperand(Record r) {
    Util.assertion(defaultOperands.containsKey(r), "Isn't an analyzed default operand?");
    return defaultOperands.get(r);
  }

  public TreePattern getPatternFragment(Record r) {
    Util.assertion(patternFragments.containsKey(r), "Invalid pattern fragment request!");
    return patternFragments.get(r);
  }

  public TreePattern getPatternFragmentIfExisting(Record r) {
    if (patternFragments.containsKey(r))
      return patternFragments.get(r);
    return null;
  }

  public TreeMap<Record, TreePattern> getPatternFragments() {
    return patternFragments;
  }

  public ArrayList<PatternToMatch> getPatternsToMatch() {
    return patternsToMatch;
  }

  private void addPatternsToMatch(PatternToMatch pat) {
    patternsToMatch.add(pat);
  }

  public DAGInstruction getInstruction(Record r) {
    Util.assertion(instructions.containsKey(r), "Undefined instruction!");
    return instructions.get(r);
  }

  public Record getIntrinsicVoidSDNode() {
    return intrinsicVoidSDNode;
  }

  public Record getIntrinsicWChainSDNode() {
    return intrinsicWChainSDNode;
  }

  public Record getIntrinsicWOChainSDNode() {
    return intrinsicWOChainSDNode;
  }

  public static TIntArrayList filterEVTs(TIntArrayList inVTs, Predicate<Integer> filter) {
    TIntArrayList result = new TIntArrayList();
    for (int i = 0, e = inVTs.size(); i != e; i++) {
      int val = inVTs.get(i);
      if (filter.test(val))
        result.add(val);
    }
    return result;
  }

  public static TIntArrayList filterVTs(TIntArrayList inVTs, Predicate<Integer> filter) {
    TIntArrayList result = new TIntArrayList();
    for (int i = 0; i < inVTs.size(); i++) {
      int vt = inVTs.get(i);
      if (filter.test(vt))
        result.add(vt);
    }
    return result;
  }

  public static TIntArrayList convertVTs(TIntArrayList inVTs) {
    TIntArrayList res = new TIntArrayList();
    inVTs.forEach(res::add);
    return res;
  }

  public static Predicate<Integer> isInteger = new Predicate<Integer>() {
    @Override
    public boolean test(Integer vt) {
      return new EVT(vt).isInteger();
    }
  };

  public static Predicate<Integer> isFloatingPoint = new Predicate<Integer>() {
    @Override
    public boolean test(Integer vt) {
      return new EVT(vt).isFloatingPoint();
    }
  };

  public static Predicate<Integer> isVector = new Predicate<Integer>() {
    @Override
    public boolean test(Integer vt) {
      return new EVT(vt).isVector();
    }
  };
}
