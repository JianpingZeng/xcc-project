/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

package utils.llc;

import backend.codegen.RegAllocLocal;
import backend.codegen.RegisterRegAlloc;
import backend.codegen.dagisel.RegisterScheduler;
import backend.codegen.dagisel.ScheduleDAGFast;
import backend.passManaging.FunctionPassManager;
import backend.support.BackendCmdOptions;
import backend.support.LLVMContext;
import backend.support.Triple;
import backend.target.*;
import backend.value.Function;
import backend.value.Module;
import tools.OutRef;
import tools.PrintStackTraceProgram;
import tools.SMDiagnostic;
import tools.Util;
import tools.commandline.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static backend.target.TargetMachine.CodeGenOpt.*;
import static tools.commandline.Desc.desc;
import static tools.commandline.FormattingFlags.Positional;
import static tools.commandline.Initializer.init;
import static tools.commandline.MiscFlags.CommaSeparated;
import static tools.commandline.OptionNameApplicator.optionName;
import static tools.commandline.ValueDesc.valueDesc;

/**
 * This class is an entry for compiling specified input LLVM assembly code into
 * machine code targeting specific machine (X86, X86_64 etc) in assembly or
 * object code.
 *
 * @author Jianping Zeng
 * @version 0.4
 */
public class LLC {
  private static final StringOpt InputFilename =
      new StringOpt(new FormattingFlagsApplicator(Positional),
          desc("<input LLVM IR file>"),
          init("-"),
          valueDesc("filename"));
  private static final StringOpt OutputFilename =
      new StringOpt(optionName("o"), desc("Override output filename"),
          valueDesc("filename"),
          init(""));

  private static final Opt<TargetMachine.CodeGenFileType> Filetype =
      new Opt<TargetMachine.CodeGenFileType>(
          new Parser<>(),
          optionName("filetype"),
          desc("Specify the type of generated file, default to 'asm'"),
          new ValueClass<>(new ValueClass.Entry<>(TargetMachine.CodeGenFileType.CGFT_AssemblyFile, "asm", "Generate assembly code"),
              new ValueClass.Entry<>(TargetMachine.CodeGenFileType.CGFT_ObjectFile, "obj(experimental)", "Generate object code")),
          init(TargetMachine.CodeGenFileType.CGFT_AssemblyFile));

  public static class OptLevelParser extends ParserUInt {
    public boolean parse(Option<?> O, String ArgName,
                         String Arg, OutRef<Integer> Val) {
      if (super.parse(O, ArgName, Arg, Val))
        return true;
      if (Val.get() > 3)
        return O.error("'" + Arg + "' invalid optimization level!");
      return false;
    }
  }

  public static CharOpt OptLevel = new CharOpt(
      new OptionNameApplicator("O"),
      desc("Optimization level. [-O0, -O1, -O2, or -O3] (default = '-O2')"),
      new FormattingFlagsApplicator(FormattingFlags.Prefix),
      new NumOccurrencesApplicator(NumOccurrences.ZeroOrMore),
      init(' '));

  public static BooleanOpt OptSize = new BooleanOpt(
      new OptionNameApplicator("Os"),
      desc("Optimize for size"),
      init(false));

  private static final BooleanOpt PrintEachModule =
      new BooleanOpt(optionName("p"),
          desc("Print module after each transformed"),
          init(false));
  private static final BooleanOpt StandardCompileOpts =
      new BooleanOpt(optionName("std-compile-opts"),
          desc("Include the standard compile time optimization"),
          init(false));
  private static final BooleanOpt DisableOptimizations =
      new BooleanOpt(optionName("disable-opt"),
          desc("Don't run any optimization passes"),
          init(false));
  private static final BooleanOpt VerifyEach =
      new BooleanOpt(optionName("verify-each"),
          desc("Verify after each transform"),
          init(false));

  public static StringOpt TargetTriple = new StringOpt(
      new OptionNameApplicator("mtriple"),
      desc("Specify target triple (e.g. x86_64-unknown-linux-gnu)"),
      init(""));

  private static StringOpt MArch = new StringOpt(
      new OptionNameApplicator("march"),
      desc("Architecture to generate code for (see --version)"),
      init(""));

  public static final StringOpt MCPU = new StringOpt(
      new OptionNameApplicator("mcpu"),
      desc("Target a specific cpu type (-mcpu=help for details)"),
      init(""));

  public static final ListOpt<String> MAttrs = new ListOpt<String>(
      new ParserString(),
      new MiscFlagsApplicator(CommaSeparated),
      new OptionNameApplicator("mattr"),
      desc("Target specific attributes"),
      valueDesc("+a1,+a2,-a3,..."));

  // FIXME, This flag would be turn off in the release.
  public static final BooleanOpt DebugMode =
      new BooleanOpt(optionName("debug"),
          desc("Enable output debug informaton"),
          init(false));

  private static final Opt<TargetMachine.RelocModel> RelocModel = new Opt<>(
      new Parser<>(),
      optionName("relocation-model"),
      desc("Choose relocation model"),
      init(TargetMachine.RelocModel.Default),
      new ValueClass<>(
          new ValueClass.Entry<>(TargetMachine.RelocModel.Default,
              "default", "Target default relocation model"),
          new ValueClass.Entry<>(TargetMachine.RelocModel.Static,
              "static", "Non-relocable code"),
          new ValueClass.Entry<>(TargetMachine.RelocModel.PIC_,
              "pic", "Fully relocable position independent code"),
          new ValueClass.Entry<>(TargetMachine.RelocModel.DynamicNoPIC,
              "dynamic-no-pic",
              "Relocable external reference, non-relocable code")
      )
  );

  private static final Opt<TargetMachine.CodeModel> CMModel = new Opt<>(
      new Parser<>(),
      optionName("code-model"),
      desc("Choose code model"),
      init(TargetMachine.CodeModel.Default),
      new ValueClass<>(
          new ValueClass.Entry<>(TargetMachine.CodeModel.Default,
              "default", "Target default code model"),
          new ValueClass.Entry<>(TargetMachine.CodeModel.Small,
              "small", "Small code model"),
          new ValueClass.Entry<>(TargetMachine.CodeModel.Kernel,
              "kernel", "Kernel code model"),
          new ValueClass.Entry<>(TargetMachine.CodeModel.Medium,
              "medium", "Medium code model"),
          new ValueClass.Entry<>(TargetMachine.CodeModel.Large,
              "large", "Large code model")
      )
  );

  /**
   * This static code block is attempted to add some desired XCCTool command line
   * options into CommandLine DataBase.
   */
  static {
    BackendCmdOptions.registerBackendCommandLineOptions();
    TargetOptions.registerTargetOptions();
  }

  private static Module theModule;

  public static void main(String[] args) throws IOException {
    String[] temp;
    if (args[0].equals("llc")) {
      temp = args;
    } else {
      temp = new String[args.length + 1];
      temp[0] = "llc";
      System.arraycopy(args, 0, temp, 1, args.length);
    }
    new PrintStackTraceProgram(temp);

    // Initialize Target machine
    TargetSelect.InitializeAllTargetInfo();
    TargetSelect.InitializeAllTarget();

    CL.parseCommandLineOptions(args, "The Compiler for LLVM IR");

    Util.DEBUG = DebugMode.value;
    OutRef<SMDiagnostic> diag = new OutRef<>();
    theModule = backend.llReader.Parser.parseAssemblyFile(InputFilename.value, diag, LLVMContext.getGlobalContext());
    if (theModule == null) {
      diag.get().print("llc", System.err);
      System.exit(0);
    }

    Triple theTriple = new Triple(theModule.getTargetTriple());
    if (theTriple.getTriple().isEmpty())
      theTriple.setTriple(tools.Process.getHostTriple());

    // Allocate target machine.  First, check whether the user has explicitly
    // specified an architecture to compile for. If so we have to look it up by
    // name, because it might be a backend that has no mapping to a target triple.
    Target theTarget = null;
    if (!MArch.value.isEmpty()) {
      for (Iterator<Target> itr = Target.TargetRegistry.iterator(); itr.hasNext(); ) {
        Target t = itr.next();
        if (t.getName().equals(MArch.value)) {
          theTarget = t;
          break;
        }
      }
      if (theTarget == null) {
        System.err.printf("llc:error: invalid target'%s'.\n", MArch.value);
        System.exit(1);
      }

      // Adjust the triple to match (if known), otherwise stick with the
      // module/host triple.
      Triple.ArchType type = Triple.getArchTypeForLLVMName(MArch.value);
      if (type != Triple.ArchType.UnknownArch)
        theTriple.setArch(type);
    }
    else {
      OutRef<String> error = new OutRef<>("");
      theTarget = Target.TargetRegistry.lookupTarget(theTriple.getTriple(), error);
      if (theTarget == null) {
        System.err.printf("llc:error: auto-selecting target for module '%s'." +
            " Please use the -march option to explicitly pick a target.\n", error.get());
        System.exit(1);
      }
    }
    // Package up features to be passed to target/subtarget
    String featureStr = "";
    if (!MCPU.value.isEmpty() || !MAttrs.isEmpty()) {
      SubtargetFeatures features = new SubtargetFeatures();
      features.setCPU(MCPU.value);
      for (int i = 0, e = MAttrs.size(); i < e; i++) {
        features.addFeature(MAttrs.get(i));
      }
      featureStr = features.getString();
    }

    TargetMachine tm = theTarget.createTargetMachine(theTriple.getTriple(), MCPU.value,
        featureStr, RelocModel.value, CMModel.value);
    Util.assertion(tm != null, "could not allocate a target machine");

    // Figure out where we should write the result file

    PrintStream os = computeOutFile(theTarget.getName());
    if (os == null) System.exit(1);

    TargetMachine.CodeGenOpt oLvl = TargetMachine.CodeGenOpt.None;
    switch (OptLevel.value) {
      default:
        System.err.println("llc: invalid optimization level.");
        System.exit(1);
        break;
      case ' ': break;
      case '0': oLvl = None; break;
      case '1': oLvl = Less; break;
      case '2': oLvl = Default; break;
      case '3': oLvl = Aggressive; break;
    }
    FunctionPassManager passes = new FunctionPassManager(theModule);

    // Add the target data from the target machine, if it exists, or the module.
    TargetData td = tm.getTargetData();
    if (td != null)
      passes.add(new TargetData(td));
    else
      passes.add(new TargetData(theModule));

    // Override default to generate verbose assembly.
    tm.setAsmVerbosityDefault(true);

    // Set the default register allocator.
    RegisterRegAlloc.setDefault(RegAllocLocal::createLocalRegAllocator);
    // Set the default instruction scheduler.
    RegisterScheduler.setDefault(ScheduleDAGFast::createFastDAGScheduler);

    if (tm.addPassesToEmitFile(passes, os, Filetype.value, oLvl)) {
        System.err.println("llc: Unable to generate this kind of file in this target!");
        if (os != System.out && os != System.err)
          Files.delete(Paths.get(OutputFilename.value));
        System.exit(1);
    }

    passes.doInitialization();
    // Run our queue of passes all at once now, efficiently.
    ArrayList<Function> functions = new ArrayList<>(theModule.getFunctionList());
    for (Function fn : functions) {
      if (!fn.isDeclaration()) {
        passes.run(fn);
      }
    }
    passes.doFinalization();
  }

  private static PrintStream computeOutFile(String targetName) {
    if (!OutputFilename.value.isEmpty()) {
      if (OutputFilename.value.equals("-"))
        return System.out;

      try {
        File f = new File(OutputFilename.value);
        if (f.exists())
          f.delete();

        f.createNewFile();
        return new PrintStream(new FileOutputStream(f));
      } catch (IOException e) {
        System.err.println(e.getMessage());
        java.lang.System.exit(-1);
      }
    }

    if (InputFilename.value.equals("-")) {
      OutputFilename.value = "-";
      return System.out;
    }

    OutputFilename.value = getFileNameRoot(InputFilename.value);
    switch (Filetype.value) {
      case CGFT_AssemblyFile: {
        switch (targetName) {
          case "c":
            OutputFilename.value += ".cbe.c";
            break;
          case "cpp":
            OutputFilename.value += ".cpp";
          default:
            OutputFilename.value += ".s";
            break;
        }
        break;
      }
      case CGFT_ObjectFile:
        OutputFilename.value += ".o";
        break;
    }

    try {
      File f = new File(OutputFilename.value);
      if (f.exists())
        f.delete();

      f.createNewFile();
      //outPath.append(f.getAbsolutePath());
      return new PrintStream(new FileOutputStream(f));
    } catch (IOException e) {
      System.err.println(e.getMessage());
      java.lang.System.exit(-1);
    }
    return null;
  }

  private static String getFileNameRoot(String file) {
    File f = new File(file);
    if (!f.exists()) {
      System.err.println("input file doesn't exists!");
      System.exit(1);
    }
    String suffix = f.getName().substring(0, f.getName().lastIndexOf('.'));
    return f.getAbsoluteFile().getParent() + "/" + suffix;
  }
}

