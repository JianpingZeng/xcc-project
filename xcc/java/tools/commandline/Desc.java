package tools.commandline;
/*
 * Extremely Compiler Collection.
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * This class implement the interface {@linkplain Modifier} to update text
 * text of the given {@code opt} for printing out human readibility information
 * when user type "-help" from console.
 *
 * @author Jianping Zeng
 * @version 0.4
 */
public class Desc implements Modifier {
  private String desc;

  public Desc(String desc) {
    this.desc = desc;
  }

  @Override
  public void apply(Option<?> opt) {
    opt.setHelpStr(desc);
  }

  /**
   * This method is a factory used for creating an instance
   * of {@linkplain Desc}.
   *
   * @param desc
   * @return
   */
  public static Desc desc(String desc) {
    return new Desc(desc);
  }
}
