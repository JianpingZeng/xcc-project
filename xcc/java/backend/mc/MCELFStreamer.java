/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2015-2020, Jianping Zeng.
 * All rights reserved.
 *
 * Please refer the LICENSE for detail.
 */

package backend.mc;
/**
 * @author Jianping Zeng.
 * @version 0.4
 */
public class MCELFStreamer extends MCObjectStreamer {

  protected MCELFStreamer(MCSymbol.MCContext ctx) {
    super(ctx);
  }
}
