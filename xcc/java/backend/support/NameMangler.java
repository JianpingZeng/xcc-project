package backend.support;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

import backend.mc.MCSymbol;
import backend.type.Type;
import backend.value.*;
import gnu.trove.map.hash.TObjectIntHashMap;
import tools.Util;

import java.util.HashMap;
import java.util.HashSet;

/**
 * This file defines a class which responsible for make asmName mangling for global
 * linkage entity of a module.
 *
 * @author Jianping Zeng
 * @version 0.4
 */
public class NameMangler {
  /**
   * This string is added to each symbol that is emitted, unless the
   * symbol is marked as not needed this prefix.
   */
  private String prefix;

  private String privatePrefix;
  private String linkerPrivatePrefix;

  /**
   * If this set, the target accepts global names in input.
   * e.g. "foo bar" is a legal asmName. This syntax is used instead
   * of escape the space character. By default, this is false.
   */
  private boolean useQuotes;

  private TObjectIntHashMap<Type> typeMap;
  private int typeCount;
  /**
   * Keep track of which global value is mangled in the current module.
   */
  private HashSet<GlobalValue> mangledGlobals;

  private int acceptableChars[];
  private final MCSymbol.MCContext context;
  /**
   * This hashmap is used to keep track the unique ID for each annotated global variable.
   */
  private HashMap<GlobalValue, Integer> annoGlobalIDs;
  private int nextAnnonGlobalId;

  public NameMangler(MCSymbol.MCContext ctx, Module m,
                     String globalPrefix,
                     String privateGlobalPrefix,
                     String linkerPrivateGlobalPrefix) {
    context = ctx;
    this.prefix = globalPrefix;
    this.privatePrefix = privateGlobalPrefix;
    this.linkerPrivatePrefix = linkerPrivateGlobalPrefix;
    useQuotes = false;
    typeMap = new TObjectIntHashMap<>();
    typeCount = 0;
    mangledGlobals = new HashSet<>();
    acceptableChars = new int[256 / 32];
    annoGlobalIDs = new HashMap<>();
    nextAnnonGlobalId = 0;

    // Letters and numbers are acceptable.
    for (char x = 'a'; x <= 'z'; x++)
      markCharAcceptable(x);

    for (char x = 'A'; x <= 'Z'; x++)
      markCharAcceptable(x);

    for (char x = '0'; x <= '9'; x++)
      markCharAcceptable(x);

    // These chars are acceptable.
    markCharAcceptable('_');
    markCharAcceptable('$');
    markCharAcceptable('.');

    HashMap<String, GlobalValue> names = new HashMap<>();
    for (Function fn : m.getFunctionList())
      insertName(fn, names);

    for (GlobalVariable gv : m.getGlobalVariableList())
      insertName(gv, names);
  }

  private void insertName(GlobalValue gv, HashMap<String, GlobalValue> names) {
    if (!gv.hasName()) return;

    // Figure out if this is already used.
    GlobalValue existingValue = names.get(gv.getName());
    if (existingValue == null)
      existingValue = gv;
    else {
      // If GV is external but the existing one is static, mangle the existing one
      if (gv.hasExternalLinkage() && !existingValue.hasExternalLinkage()) {
        mangledGlobals.add(existingValue);
        existingValue = gv;
      } else if (gv.hasExternalLinkage() && existingValue.hasExternalLinkage()
          && gv.isDeclaration() && existingValue.isDeclaration()) {
        // If the two globals both have external inkage, and are both external,
        // don't mangle either of them, we just have some silly type mismatch.
      } else {
        // otherwise, mangle gv.
        mangledGlobals.add(gv);
      }
    }
  }

  private int getTypeID(Type ty) {
    int e = typeMap.get(ty);
    if (e == 0) e = ++typeCount;
    return e;
  }

  public MCSymbol getSymbol(GlobalValue gv) {
    String name = getMangledNameWithPrefix(gv, false);
    return context.getOrCreateSymbol(name);
  }

  public enum ManglerPrefixTy {
    /**
     * Emit default string before each symbol.
     */
    Default,
    /**
     * Emit "private" prefix before each symbol.
     */
    Private,
    /**
     * Emit "linker private" prefix before each symbol.
     */
    LinkerPrivate
  }

  public String getMangledName(GlobalValue gv, String suffix, boolean forceprivate) {
    Util.assertion(!(gv instanceof Function) || !((Function) gv).isIntrinsicID(),
        String.format("Intrinsic function '%s' shouldn't be mangled!", gv.getName()));
    ManglerPrefixTy prefixTy =
        (gv.hasPrivateLinkage() || forceprivate) ? ManglerPrefixTy.Private
            : gv.hasLinkerPrivateLinkage() ? ManglerPrefixTy.LinkerPrivate :
            ManglerPrefixTy.Default;
    String resName;
    if (gv.hasName())
      resName = makeNameProper(gv.getName() + suffix, prefixTy);
    else {
      // get the unique ID for the annotated variable.
      int id;
      if (annoGlobalIDs.containsKey(gv))
        id = annoGlobalIDs.get(gv);
      else {
        id = nextAnnonGlobalId++;
        annoGlobalIDs.put(gv, id);
      }
      // must mangle the global into an unique id.
      resName = getMangledNameWithPrefix("__unnamed_" + id, prefixTy);
    }
    // if we are supposed to add a microsoft-style suffix for stdcall/fastcall, add it.
    return resName;
  }

  public String getMangledName(GlobalValue gv) {
    return getMangledName(gv, "", false);
  }

  public String getMangledNameWithPrefix(String name) {
    return getMangledNameWithPrefix(name, ManglerPrefixTy.Default);
  }
  public String getMangledNameWithPrefix(String name,
                                         ManglerPrefixTy prefixTy) {
    return makeNameProper(name, prefixTy);
  }

  public String getMangledNameWithPrefix(GlobalValue gv,
                                  boolean isImplicitlyPrivate) {
    return getMangledName(gv, "", isImplicitlyPrivate);
  }


  private static char hexDigit(int v) {
    return v < 10 ? Character.forDigit(v, 10) : (char) (v - 10 + (int) 'A');
  }

  private String mangleLetter(char digit) {
    return "_" + hexDigit(digit >> 4) + hexDigit(digit & 15) + "_";
  }

  private String makeNameProper(String origin, ManglerPrefixTy prefixTy) {
    Util.assertion(!origin.isEmpty(), "Can't mangle an empty string");

    StringBuilder result = new StringBuilder();
    if (!useQuotes) {
      int i = 0;
      boolean needsPrefix = true;
      if (origin.startsWith("\\01")) {
        needsPrefix = false;
        i += 3;
      }
      // Mangle the first letter specially, don't allow numbers.
      if (origin.charAt(i) >= '0' && origin.charAt(i) <= '9')
        result.append(mangleLetter(origin.charAt(i++)));

      for (; i < origin.length(); i++) {
        char ch = origin.charAt(i);
        if (!isCharAcceptable(ch))
          result.append(mangleLetter(ch));
        else
          result.append(ch);
      }
      if (needsPrefix) {
        result.insert(0, prefix);
        if (prefixTy == ManglerPrefixTy.Private)
          result.insert(0, privatePrefix);
        else if (prefixTy == ManglerPrefixTy.LinkerPrivate)
          result.insert(0, linkerPrivatePrefix);
      }
      return result.toString();
    }
    boolean needsPrefix = true;
    boolean needsQuotes = false;
    int i = 0;
    if (origin.charAt(i) == '1') {
      needsPrefix = false;
      ++i;
    }

    if (origin.charAt(i) >= '0' && origin.charAt(i) <= '9')
      needsQuotes = true;
    // Do an initial scan of the string, checking to see if we need quotes or
    // to escape a '"' or not.
    if (!needsQuotes) {
      for (; i < origin.length(); i++) {
        char ch = origin.charAt(i);
        if (!isCharAcceptable(ch)) {
          needsQuotes = true;
          break;
        }
      }
    }

    if (!needsQuotes) {
      if (!needsPrefix)
        return origin.substring(1);

      result = new StringBuilder(prefix + origin);
      if (prefixTy == ManglerPrefixTy.Private)
        result.insert(0, privatePrefix);
      else if (prefixTy == ManglerPrefixTy.LinkerPrivate)
        result.insert(0, linkerPrivatePrefix);

      return result.toString();
    }

    if (needsPrefix)
      result = new StringBuilder(origin.substring(0, i));

    // Otherwise, construct the string in expensive way.
    for (; i < origin.length(); i++) {
      char ch = origin.charAt(i);
      if (ch == '"')
        result.append("_QQ_");
      else if (ch == '\n')
        result.append("_NL_");
      else
        result.append(ch);
    }
    if (needsPrefix) {
      result.insert(0, prefix);
      if (prefixTy == ManglerPrefixTy.Private)
        result.insert(0, privatePrefix);
      else if (prefixTy == ManglerPrefixTy.LinkerPrivate)
        result.insert(0, linkerPrivatePrefix);
    }
    result = new StringBuilder('"' + result.toString() + '"');
    return result.toString();
  }

  private void markCharAcceptable(char x) {
    acceptableChars[x / 32] |= 1 << (x & 31);
  }

  private void markCharUnacceptale(char x) {
    acceptableChars[x / 32] &= ~(1 << (x & 31));
  }

  private boolean isCharAcceptable(char ch) {
    return (acceptableChars[ch / 32] & (1 << (ch & 31))) != 0;
  }

  public String makeNameProperly(String name) {
    return null;
  }

  public void setUseQuotes(boolean val) {
    useQuotes = val;
  }
}
