package backend.codegen;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2018, Jianping Zeng.
 * All rights reserved.
 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import backend.mc.MCInstrDesc;
import backend.pass.FunctionPass;
import backend.support.MachineFunctionPass;
import backend.target.TargetInstrInfo;
import backend.target.TargetLowering;

import java.util.Objects;

/**
 * This pass is used to expand some pseudo instructions generated in isel phase.
 * @author Jianping Zeng.
 * @version 0.4
 */
public class ExpandISelPseudo extends MachineFunctionPass {
  @Override
  public boolean runOnMachineFunction(MachineFunction mf) {
    boolean changed = false;
    TargetInstrInfo tii = mf.getSubtarget().getInstrInfo();
    TargetLowering tli = mf.getSubtarget().getTargetLowering();

    for (int i = 0; i < mf.getNumBlocks(); i++) {
      MachineBasicBlock mbb = mf.getMBBAt(i);
      for (int j = 0, sz = mbb.size(); j < sz; ++j) {
        MachineInstr mi = mbb.getInstAt(j);
        MCInstrDesc mid = tii.get(mi.getOpcode());
        if (mid.usesCustomInsertionHook()) {
          MachineBasicBlock newMBB = tli.emitInstrWithCustomInserter(mi, mbb);
          if (!Objects.equals(newMBB, mbb)) {
            i = mf.getIndexOfMBB(newMBB);
            mbb = newMBB;
            j = 0;
            sz = newMBB.size();
            changed = true;
          }
        }
      }
    }
    return changed;
  }

  @Override
  public String getPassName() {
    return "Expand ISel Pseudo-instructions";
  }

  public static FunctionPass createExpandISelPseudoPass() {
    return new ExpandISelPseudo();
  }
}
