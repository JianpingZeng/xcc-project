package backend.codegen;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * @author Jianping Zeng
 * @version 0.4
 */
public class MachineMove {
  private int LabelID;                     // Label ID number for post-instruction
  // address when result of move takes
  // effect.
  MachineLocation Destination;          // Move to location.
  MachineLocation Source;               // Move from location.

  public MachineMove() {
    LabelID = (0);
    Destination = new MachineLocation();
    Source = new MachineLocation();
  }

  public MachineMove(int ID, MachineLocation D, MachineLocation S) {
    LabelID = ID;
    Destination = new MachineLocation(D);
    Source = new MachineLocation(S);
  }

  // Accessors
  public int getLabelID() {
    return LabelID;
  }

  public MachineLocation getDestination() {
    return Destination;
  }

  public MachineLocation getSource() {
    return Source;
  }
}
