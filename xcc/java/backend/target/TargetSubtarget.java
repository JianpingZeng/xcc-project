package backend.target;
/*
 * Extremely Compiler Collection
 * Copyright (c) 2015-2020, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

import backend.codegen.dagisel.SDep;
import backend.codegen.dagisel.SUnit;
import backend.mc.MCSubtargetInfo;

/**
 * @author Jianping Zeng
 * @version 0.4
 */
public abstract class TargetSubtarget extends MCSubtargetInfo {
  protected TargetInstrInfo instrInfo;
  protected TargetRegisterInfo regInfo;

  public int getSpecialAddressLatency() {
    return 0;
  }

  public boolean isTargetDarwin() {
    return false;
  }

  public boolean isTargetELF() {
    return false;
  }

  public boolean isTargetCygMing() {
    return false;
  }

  public boolean isTargetWindows() {
    return false;
  }

  public boolean isTargetCOFF() {
    return isTargetCygMing() || isTargetWindows();
  }

  public void adjustSchedDependency(SUnit opSU, SUnit su, SDep dep) {
  }

  public int getHwMode() {
    return 0;
  }

  public TargetRegisterInfo getRegisterInfo() {
    return regInfo;
  }

  public TargetInstrInfo getInstrInfo() {
    return instrInfo;
  }

  public abstract TargetFrameLowering getFrameLowering();

  public abstract TargetLowering getTargetLowering();

  /**
   * Parses the sub-features string specified by subtarget options.
   * This function should by overrided by any tablegen generated sub class.
   * @param fs
   * @param cpu
   */
  public abstract void parseSubtargetFeatures(String fs, String cpu);

  public boolean is64Bit() {
    return false;
  }
}
