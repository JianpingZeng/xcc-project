package backend.target.x86;
/*
 * Extremely C language Compiler
 * Copyright (c) 2015-2019, Jianping Zeng.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

import backend.target.Target;

/**
 * X86 32-bit target machine.
 *
 * @author Jianping Zeng
 * @version 0.4
 */
public class X86_32TargetMachine extends X86TargetMachine {
  public X86_32TargetMachine(Target t, String triple, String cpu, String fs,RelocModel rm, CodeModel cm) {
    super(t, triple, cpu, fs, rm, cm, false);
  }
}
